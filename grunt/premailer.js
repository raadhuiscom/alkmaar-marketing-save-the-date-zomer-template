module.exports = {
	main: {
    	options: {
      		verbose: true,
      		removeComments: true
    	},
    	files: {
      		'dist/savethedate-inline.html': ['dist/savethedate.html'],
      		'dist/uitnodiging-inline.html': ['dist/uitnodiging.html'],
      		'dist/algemeen-inline.html': ['dist/algemeen.html']
    	}
  	}
};